﻿using UnityEngine;

namespace TelemachusReimagined
{
    /// <summary>
    /// Handles all UI functionality
    /// </summary>
    public static class TelemachusUi
    {
        private static Rect _windowPos = new Rect(Screen.width * 0.75f, Screen.height * 0.75f, 300, 80);
        
        /// <summary>
        /// Shows a gui window
        /// </summary>
        /// <param name="instanceId">The instance the window belongs to</param>
        public static void Gui(int instanceId)
        {
            _windowPos = GUILayout.Window(instanceId, _windowPos, WindowGui, "Telemachus Reimagined");
        }

        /// <summary>
        /// Is called to render the window contents
        /// </summary>
        /// <param name="windowID">Idk, i just have to accept an int</param>
        private static void WindowGui(int windowID)
        {
            // The window is seperated into two rows
            GUILayout.BeginVertical();
            
            // The first row
            GUILayout.BeginHorizontal();
            GUILayout.Label("Port:");
            GUILayout.FlexibleSpace();
            if (TelemachusReimagined.Enabled)
            {
                // If it currently is enabled changing ports would not be a good idea so just show it in a label
                GUILayout.Label(TelemachusReimagined.Port.ToString());
            }
            else
            {
                // If it is disabled, the user can change ports
                var portString = GUILayout.TextField(TelemachusReimagined.Port.ToString(), 5);
                if (int.TryParse(portString, out var outInt))
                {
                    // If the user has inputted a valid int, set the new port
                    TelemachusReimagined.Port = outInt;
                }
            }
            GUILayout.EndHorizontal();
            
            // The second row
            GUILayout.BeginHorizontal();
            GUILayout.Label("Toggle Telemachus:");
            GUILayout.FlexibleSpace();
            if (GUILayout.Button(TelemachusReimagined.Enabled ? "On" : "Off"))
            {
                TelemachusReimagined.Enabled = !TelemachusReimagined.Enabled;
            }
            GUILayout.EndHorizontal();
            
            GUILayout.EndVertical();
            
            // The user should be able to move the window
            GUI.DragWindow();
        }
    }
}