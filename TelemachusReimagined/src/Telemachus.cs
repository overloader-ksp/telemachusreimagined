﻿using KSP.UI.Screens;
using UnityEngine;

namespace TelemachusReimagined
{
    /// <summary>
    /// The main class of this plugin that handles everything
    /// </summary>
    [KSPAddon(KSPAddon.Startup.SpaceCentre,
        true)] // The plugin should start once a save is loaded and then keep on running
    public class TelemachusReimagined : MonoBehaviour
    {
        /// <summary>
        /// The Texture of the button in the toolbar
        /// </summary>
        private const string ButtonTexturePath = "TelemachusReimagined/Textures/button";

        /// <summary>
        /// The Port to listen for connections
        /// </summary>
        public static int Port = 3060;

        /// <summary>
        /// If the plugin is currently turned on
        /// </summary>
        public static bool Enabled = false;

        /// <summary>
        /// If the mods ui should be shown
        /// </summary>
        private bool _showUi;

        /// <summary>
        /// Stuff that is initialized when the plugin is loaded
        /// </summary>
        private void Start()
        {
            ApplicationLauncher.Instance.AddModApplication( // Create a button in the toolbar
                () => _showUi = true, // That shows the ui on toggle on
                () => _showUi = false, // Hides the ui on toggle off
                () => { }, // Nothing on hover
                () => { }, // Nothing on hover out
                () => { }, // Nothing on enable
                () => { }, // Nothing on disable
                ApplicationLauncher.AppScenes.ALWAYS, // Is shown in all scenes
                GameDatabase.Instance.GetTexture(ButtonTexturePath, false)); // Uses this texture
        }

        /// <summary>
        /// This function handles rendering the ui
        /// </summary>
        private void OnGUI()
        {
            if (_showUi) // Only render the ui if it is actually toggled on
            {
                TelemachusUi.Gui(GetInstanceID());
            }
        }
    }
}